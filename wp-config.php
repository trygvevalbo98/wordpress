<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'p^@o&&{0MLi5V/0XIY/l=s2MPt3*gsM)PELI?@237S7SW:}R#?YG8YBC5rrDQJJ$');
define('SECURE_AUTH_KEY',  ')N.b7K).L-~rVL!a0eJuI{M0UAxp39-n6x[lPNM4)skM%t>*pxu[u(~9ovC^uEFq');
define('LOGGED_IN_KEY',    '5~.o0Q,Lqrj!:JQlfi&vP3JvKIW/}[[4p7l`Yz7*:k<jLO.q$]gyqTe^hx=H8b^x');
define('NONCE_KEY',        'e: 1L:uwaO%:5pkp0k`Iq* bau}}RwL4(}!L3/Jrv4!]pE[xA>:(p0:TT#mhlz5_');
define('AUTH_SALT',        'A2])w!f2L!9?.2Y#Lr>bCRJsz@}OoVv[<IsmAu61X=19cszu|L>DfE(6yF)P11[9');
define('SECURE_AUTH_SALT', 'dwx#GP||nXj?&D?aCyh6s{J#_zXj!I6 -FSf@0qeUM%~D}SmZ#8eD5Ra-L~<48Xc');
define('LOGGED_IN_SALT',   'DJ8-X|alT/xuY-y-V*2nF%_RA#3rT) P8/Z,a.f+]_$OQa:1q-`x4Amf_b5{<w87');
define('NONCE_SALT',       'MN7C0FV_63/A8h.#y`}vP;==^T%o6mR:M0Aze[VZ%N],|f{RmqT?k,(qb4/}a{HQ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
